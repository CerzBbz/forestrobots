package objects;

public class Robot extends Object {

	static String name = "robot";
	
	public enum Size{Large, Small};
	
	private Size size;
	
	public Robot(int id, int x, int y, Size size) {
		super(id, x, y);
		this.size = size;
	}

	public String getAgentName()
	{
		return name + getId();
	}
	
	public Size getSize()
	{
		return size;
	}
}
