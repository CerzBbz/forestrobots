package objects;

import jason.asSyntax.Literal;

public class PointOfInterest extends Object {
	private static String name = "poi";
	private boolean blocked;

	public PointOfInterest(int id, int x, int y, boolean blocked) {
			super(id, x, y);
			this.blocked = blocked;
	}

	public Literal getBlockedLiteral() {
		String s = "blocked(" + getName() + ")";
		if (isBlocked()) {
			return Literal.parseLiteral(s);
		} else {
			return Literal.parseLiteral("~" + s);
		}
	}

	public Literal getAtLiteral() {
		return Literal.parseLiteral("at(" + getName() + "," + getX() + "," + getY() + ")");
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public String getName() {
		return name + getId();
	}

	public void setName(String name) {
		this.name = name;
	}
}