package plannedWorld;

import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Structure;
import jason.environment.Environment;
import jason.environment.grid.GridWorldModel;
import jason.environment.grid.GridWorldView;
import jason.environment.grid.Location;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

import objects.PointOfInterest;

public class ForestEnvironment extends Environment {
	static final int O = 4; // OBSTACLE code for grid model 
	static final int P = 8; // POI code for grid model
	static final int B = 16; // BLOCKED_POI code for grid model
	static final int R = 32; // ROAD code for grid model

	static final int GRID_SIZE = 10;

	int numRobots = 2;
	
	int[][] map = new int[][] { 
			{ P, R, R, R, R, B, O, O, O, O },
			{ R, O, O, O, O, R, O, O, O, O },
			{ R, O, O, R, R, R, O, O, R, B },
			{ R, B, R, R, O, R, R, R, R, O },
			{ O, R, O, O, O, O, R, O, O, O },
			{ R, R, O, O, O, R, R, R, O, O },
			{ R, O, O, O, O, R, O, R, B, O },
			{ B, R, R, R, O, R, O, O, R, O },
			{ O, O, O, R, R, R, R, R, R, O },
			{ O, O, O, O, O, O, O, O, R, P } };



	ArrayList<PointOfInterest> pois = new ArrayList<PointOfInterest>();

	ForestModel model;
	ForestView view;

	public void init(String[] args) {
		model = new ForestModel();
		view = new ForestView(model);
		model.setView(view);
		updatePercepts();
	}
	
	void updatePercepts() {
		clearPercepts();

		// Let agents and robots know about their positions
		for (int i = 0; i < numRobots; i++) {
			Location robotLocation = model.getAgPos(i);
			Literal at = Literal.parseLiteral("at(robot" + (i + 1) + ","
					+ robotLocation.x + "," + robotLocation.y + ")");
			addPercept(at); // Making it a global perception lets us clear it
							// after movements occur
		}

		for (PointOfInterest p : pois) {
			Literal at = p.getAtLiteral();
			addPercept(at);

			Literal blocked = p.getBlockedLiteral();
			addPercept(blocked);
		}
	}
	
	@Override
	public boolean executeAction(String ag, Structure action) {
		try {
			if (action.getFunctor().equals("move_towards")) {
				int x = (int) ((NumberTerm) action.getTerm(0)).solve();
				int y = (int) ((NumberTerm) action.getTerm(1)).solve();
				int id = Integer.parseInt("" + ag.charAt(ag.length() - 1));
				id--;
				
				model.moveTowards(id, x, y);
				
			} else if (action.getFunctor().equals("clear")) {
				String poiName = action.getTerm(0).toString();
				int x = (int) ((NumberTerm) action.getTerm(1)).solve();
				int y = (int) ((NumberTerm) action.getTerm(2)).solve();

				model.clearPoi(poiName, x, y);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		updatePercepts();

		try {
			Thread.sleep(200);
		} catch (Exception e) {
		}
		informAgsEnvironmentChanged();
		return true;
	}

	
	/**
	 * ForestModel This is the Model, and is where the scenario will be
	 * initialised.
	 * 
	 */
	class ForestModel extends GridWorldModel {
		public ForestModel() {
			super(GRID_SIZE, GRID_SIZE, numRobots);

			setAgPos(0, 0, 0); // One Robot starts at top left
			setAgPos(1, GRID_SIZE - 1, GRID_SIZE - 1); // Other Robot starts at
														// bottom right
			
			int id = 0;
			int x = 0;
			int y = 0;
			
			//Plot the rest of the items from the map
			for (int[] row : map) {
				for (int i : row) {
					switch(i)
					{
					case P: pois.add(new PointOfInterest(id, x, y, false)); id++; add(i,x,y); break;
					case B: pois.add(new PointOfInterest(id, x, y, true)); id++; add(i,x,y); break;
					default: add(i, x, y);
					}
					x++;
				}
				x = 0;
				y++;
			}
		}
		
		void moveTowards(int id, int x, int y) throws Exception {

			Location robot = getAgPos(id);
			
			ArrayList<Location> locations = new ArrayList<Location>();
			
			/**
			 * Moving done randomly until I make a pathfinding algorithm
			 */
			if(isFreeOfObstacle(robot.x + 1, robot.y))
			{
				locations.add(new Location(robot.x + 1, robot.y));
			}
			if(isFreeOfObstacle(robot.x - 1, robot.y))
			{
				locations.add(new Location(robot.x - 1, robot.y));
			}
			if(isFreeOfObstacle(robot.x, robot.y + 1))
			{
				locations.add(new Location(robot.x, robot.y + 1));
			}
			if(isFreeOfObstacle(robot.x, robot.y - 1))
			{
				locations.add(new Location(robot.x, robot.y - 1));
			}
			
			Random r = new Random();
			int i = r.nextInt(locations.size());
			
			setAgPos(id, locations.get(i));
		}

		void clearPoi(String name, int x, int y) throws Exception {		
			for (PointOfInterest p : pois) {
				if (p.getName().equals(name)) {
					p.setBlocked(false);
				}
			}

			remove(B,x,y);
			add(P, x, y);			
		}
	}

	/**
	 * ForestView This is the view for the ForestEnviornment which will be
	 * displayed to the user.
	 * 
	 */
	class ForestView extends GridWorldView {
		public ForestView(ForestModel model) {
			super(model, "Forest", 600);
			defaultFont = new Font("Arial", Font.BOLD, 18);
			setVisible(true);
			repaint();
		}

		@Override
		public void draw(Graphics g, int x, int y, int object) {
			switch (object) {
			case ForestEnvironment.B:
				drawBlocked(g, x, y);
				break;
			case ForestEnvironment.P:
				drawPOI(g, x, y);
				break;
			case ForestEnvironment.R:
				drawRoad(g, x, y);
				break;
			}
		}

		@Override
		public void drawAgent(Graphics g, int x, int y, Color c, int id) {
			c = Color.cyan;
			g.setColor(Color.black);
			super.drawAgent(g, x, y, c, id);
		}

		public void drawBlocked(Graphics g, int x, int y) {
			g.setColor(Color.red);
			g.fillRect(x * cellSizeW, y * cellSizeH, cellSizeW, cellSizeH);
			g.setColor(Color.black);
			String label = "POI";
			for(PointOfInterest pointOfInterest : pois)
			{
				if(pointOfInterest.getX() == x && pointOfInterest.getY() == y)
				{
					label = label + pointOfInterest.getId();
				}
			}
			super.drawString(g, x, y, defaultFont, label);
		}

		public void drawPOI(Graphics g, int x, int y) {
			g.setColor(Color.blue);
			g.fillRect(x * cellSizeW, y * cellSizeH, cellSizeW, cellSizeH);
			g.setColor(Color.white);
			String label = "POI";
			for(PointOfInterest pointOfInterest : pois)
			{
				if(pointOfInterest.getX() == x && pointOfInterest.getY() == y)
				{
					label = label + pointOfInterest.getId();
				}
			}
			super.drawString(g, x, y, defaultFont, label);
		}

		public void drawRoad(Graphics g, int x, int y) {
			g.setColor(Color.gray);
			g.fillRect(x * cellSizeW, y * cellSizeH, cellSizeW, cellSizeH);
			String label = "Road";
			super.drawString(g, x, y, defaultFont, label);
		}
	}
}
