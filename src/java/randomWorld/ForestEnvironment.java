package randomWorld;

import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Structure;
import jason.environment.Environment;
import jason.environment.grid.GridWorldModel;
import jason.environment.grid.GridWorldView;
import jason.environment.grid.Location;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

import objects.PointOfInterest;

public class ForestEnvironment extends Environment {

	public int numRobots = 2;
	public int numPois = 10;
	public int gridSize = numPois * 2;

	public static final int POI = 8; // POI code for grid model
	public static final int BLOCKED = 16; // Blocked code for grid model

	private ForestModel model;
	private ForestView view;

	ArrayList<PointOfInterest> POIs = new ArrayList<PointOfInterest>();

	@Override
	public void init(String[] args) {
		if(args.length > 0)
		{
			numRobots = Integer.parseInt(args[0]);
		} 
		if(args.length > 1)
		{
			numPois = Integer.parseInt(args[1]);
			gridSize = numPois * 2;
		}
		
		model = new ForestModel();
		view = new ForestView(model);
		model.setView(view);
		updatePercepts();
	}

	@Override
	public boolean executeAction(String ag, Structure action) {
		try {
			if (action.getFunctor().equals("move_towards")) {
				int x = (int) ((NumberTerm) action.getTerm(0)).solve();
				int y = (int) ((NumberTerm) action.getTerm(1)).solve();
				int id = Integer.parseInt("" + ag.charAt(ag.length() - 1));
				id--;
				
				model.moveTowards(id, x, y);
				
			} else if (action.getFunctor().equals("clear")) {
				String poiName = action.getTerm(0).toString();
				int x = (int) ((NumberTerm) action.getTerm(1)).solve();
				int y = (int) ((NumberTerm) action.getTerm(2)).solve();

				model.clearPoi(poiName, x, y);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		updatePercepts();

		try {
			Thread.sleep(200);
		} catch (Exception e) {
		}
		informAgsEnvironmentChanged();
		return true;
	}

	void updatePercepts() {
		clearPercepts();

		// Let agents and robots know about their positions
		for (int i = 0; i < numRobots; i++) {
			Location robotLocation = model.getAgPos(i);
			Literal at = Literal.parseLiteral("at(robot" + (i + 1) + ","
					+ robotLocation.x + "," + robotLocation.y + ")");
			addPercept(at); // Making it a global perception lets us clear it
							// after movements occur
		}

		for (PointOfInterest p : POIs) {
			Literal at = p.getAtLiteral();
			addPercept(at);

			Literal blocked = p.getBlockedLiteral();
			addPercept(blocked);
		}
	}

	@Override
	public void stop() {
		super.stop();
	}

	class ForestModel extends GridWorldModel {
		private ForestModel() {
			super(gridSize, gridSize, numRobots);
			Random r = new Random();
			// Initial Robot Positions
			try {
				for (int i = 0; i < numRobots; i++) {
					int x = r.nextInt(gridSize);
					int y = r.nextInt(gridSize);
					Location robotLocation = new Location(x, y);
					setAgPos(i, robotLocation);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Generate the Blockades
			for (int i = 0; i < numPois; i++) {
				int x = r.nextInt(gridSize);
				int y = r.nextInt(gridSize);
//				boolean b = r.nextBoolean();
				boolean b = true;

				if (b) {
					add(BLOCKED, x, y);
				}
				else
				{
					add(POI, x, y);
				}

				PointOfInterest poi = new PointOfInterest(i, x, y, b);
				POIs.add(poi);
			}
		}

		void moveTowards(int id, int x, int y) throws Exception {

			Location robot = getAgPos(id);
			if (robot.x < x)
				robot.x++;
			else if (robot.x > x)
				robot.x--;
			if (robot.y < y)
				robot.y++;
			else if (robot.y > y)
				robot.y--;

			setAgPos(id, robot);
		}

		void clearPoi(String name, int x, int y) throws Exception {		
			for (PointOfInterest p : POIs) {
				if (p.getName().equals(name)) {
					p.setBlocked(false);
				}
			}

			remove(BLOCKED,x,y);
			add(POI, x, y);			
		}
	}

	@SuppressWarnings("serial")
	class ForestView extends GridWorldView {
		public ForestView(ForestModel model) {
			super(model, "Forest", 600);
			defaultFont = new Font("Arial", Font.BOLD, 18);
			setVisible(true);
			repaint();
		}

		@Override
		public void draw(Graphics g, int x, int y, int object)
		{		
			switch(object)
			{
			case BLOCKED : drawBlocked(g,x,y); break;
			case POI : drawPOI(g,x,y); break;
			}
		}

		@Override
		public void drawAgent(Graphics g, int x, int y, Color c, int id) {		
			c = Color.cyan;
			g.setColor(Color.black);
			super.drawAgent(g,x,y,c,id);
		}
		
		public void drawBlocked(Graphics g, int x, int y) {
			g.setColor(Color.red);
			String label = "POI";
			super.drawString(g, x, y, defaultFont, label);
		}

		public void drawPOI(Graphics g, int x, int y) {
			g.setColor(Color.black);
			String label = "POI";
			super.drawString(g, x, y, defaultFont, label);
		}
	}
}