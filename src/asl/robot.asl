// Agent robot in project forestRobots

/* Initial beliefs and rules */

/* Initial goals */

!inform.

/* Plans */

+!inform
<- 	.my_name(N);
	.print("Letting the coordinator know that I am free");
	.send(coordinator, tell, free(N)).

+!clear(P) : .my_name(N) & at(P, X, Y) & at(N,X,Y) & not P=N 
<-	-assigned(P)[source(coordinator)];
	.print("Clearing: (", P, ")");
	.send(coordinator, achieve, update_beliefs(N,P));
	!inform;
	clear(P,X,Y).
	

+!clear(P) : .my_name(N) & at(P, X, Y) & not at(N, X, Y) & not P=N
<-	!move(X,Y).

+!clear(P) : true 
<- 	!clear(P).
	
+!move(X,Y) : .my_name(N) & at(N,X,Y) & assigned(Z) 
<-  .print("arrived at coordinates: (", X, ",", Y, ")"); 	
	!clear(Z).

+!move(X,Y) : .my_name(N) & not at(N,X,Y)
<- 	.print("moving towards coordinates: (", X, ",", Y, ")");
	move_towards(X,Y);
	!move(X,Y).