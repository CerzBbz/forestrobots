// Agent planner in project forestRobots

/* Initial beliefs and rules */
//Sensed from ForestEnviornment

/* Initial goals */
!assign_tasks.

/* Plans */

/* If there is a free robot, and blocked path which is unassigned
 * Add the belief that we have assign that robot to the path
 * Remove the belief that the robot is free
 * Send the robot the new goal of clearing the path
 * Send the robot the new belief that it has been assigned that POI
 */
+!assign_tasks : free(R) & blocked(P) & not assigned(_,P)
<- 	.print("Assigning ", R, " to unblock ", P );
	+assigned(R,P); 
	-free(R)[source(R)]; 
	.send(R, achieve, clear(P));
	.send(R, tell, assigned(P));
	!assign_tasks.

+!assign_tasks : not free(_)
<-	.print("Waiting on robots");
	.wait(1000);
	!assign_tasks.

+!assign_tasks : blocked(P) & assigned(_,P) & free(R)
<-	.wait(1000);
	!assign_tasks.
	
+!assign_tasks : not blocked(_) 		 
<-	.print("No blockades remaining"). //Not re-adding !plan as we have no more blockages to account for currently
	
+!update_beliefs(N,P) : true
<- -assigned(N,P)[source(_)].